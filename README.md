![Build Status](https://gitlab.inria.fr/timesquare/timesquare.gitlabpages.inria.fr/badges/master/pipeline.svg)

---

Build the https://timesquare.gitlabpages.inria.fr site

---


The main purpose is to store and deploy the various version of TimeSquare update site

New updatesite releases are automatically pushed in this repository from https://gitlab.inria.fr/jdeanton/TimeSquare when a tag is added (or modified) (cf. https://gitlab.inria.fr/jdeanton/TimeSquare/-/blob/master/.gitlab-ci.yml)

It uses a simple bootstrap presentation.

It uses a bash script (`scripts/make_index.html`) to generate an index.html allowing to visualize the content of the folders. (in this case the content of the /updatesite folders )

If a `.content_desc.html` file exists in a folder, the content of this file will be used to as description of the folder. 
 
