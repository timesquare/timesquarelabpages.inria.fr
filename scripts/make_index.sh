#!/bin/bash
# BASH script designed to generate index files using 
# Twitter Bootstrap [getbootstrap.com]

# scripts from https://blog.sleeplessbeastie.eu/2013/11/21/how-to-generate-directory-index-files/

# Notes:
# * Script will ignore "img" and "css" directories and "index.html" file
# * Remember to copy Twitter Bootstrap files (css,img)

# Sample usage:
# cd /var/www/download
# make_index.sh

# web root directory, 
# it will be used to generate URLs
# ""           - root directory
# "/download" - '/download' subdirectory
root_directory=""
root_name=

# template settings
brand="TimeSquare"
title="TimeSquare"

# generate header part of the HTML file
function header {
  cat << EOF
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>$title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	
	<!-- fontawesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	
  </head>

  <body>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
	  	<!-- Brand -->
	  	<a class="navbar-brand" href="/">
	  		<img src="/image/tsq.png" alt="Logo" style="width:100px;">
	  	</a>  
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    	<span class="navbar-toggler-icon"></span>
	  	</button>
		<div class="collapse navbar-collapse" id="navbarNav">
		    <ul class="navbar-nav">
		      <li class="nav-item active">
		        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="/updatesite">Update Sites</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="https://gitlab.inria.fr/jdeanton/TimeSquare">Sources</a>
		      </li>
		    </ul>
	  	</div>
	</nav>
    <div class="container"> 
      <div class="page-header"><h1>$title</h1></div>
EOF
}

# generate footer part of the HTML file
function footer {
  cat << EOF
    </div> <!-- /container -->
  </body>
</html>
EOF
}


# create index file for each directory (ignore ./css and ./img directories)
find . -type d -not -path "*/img" -not -path "*/css" | while read directory; do
  # remove "./" from the beginning of the path
  directory=${directory#./}
  root_dir_name=$(basename `pwd`)
  
  # index file
  index_file=$directory/index.html

  # create/truncate index file
  : > $index_file

  # append header to the index file
  header >> $index_file

  # append "Up to higher level directory" link
  echo "<div class=\"row\">" >> $index_file
  if [ -f "$directory/.content_desc.html" ]; then
     folder_description=$(cat "$directory/.content_desc.html")
  fi
  if [ "$directory" != "." ]; then
    echo "<div class=\"col-sm-4\"><h2 class=\"muted\"><i class=\"far fa-folder-open\"></i> ${directory}</h2></div><div class=\"col-sm-8\">$folder_description</div>" >> $index_file    
    echo "<div class=\"col-sm-12\"><hr/></div>" >> $index_file
    echo "<div class=\"col-sm-12\"><i class=\"fas fa-arrow-up\"></i> <a href=\"../index.html\">Up to higher level directory</a></div>" >> $index_file
  else
    echo "<div class=\"col-sm-4\"><h2 class=\"muted\"><i class=\"far fa-folder-open\"></i>${root_dir_name}</h2></div><div class=\"col-sm-8\">$folder_description</div>" >> $index_file
    echo "<div class=\"col-sm-12\"><hr/></div>" >> $index_file
  #  echo "<i class=\"fas fa-arrow-up\"></i> Up to higher level directory" >> $index_file
  fi
  echo "</div>" >> $index_file

  # append empty row
  echo "<div class=\"row\"><div class=\"col-sm-12\"><br/></div></div>" >> $index_file

  # generate content
  content=""
  while read file
  do
    if [ -d "$directory/$file" ]; then
      content+="<div class=\"row\">"
      if [ "$directory" == "." ]; then
        content+="<div class=\"col-sm-10\"><div class=\"directory\"><i class=\"fas fa-arrow-right\"></i> <a href=\"$root_directory/$file/index.html\">$file</a></div></div>"
      else
        content+="<div class=\"col-sm-10\"><div class=\"directory\"><i class=\"fas fa-arrow-right\"></i> <a href=\"$root_directory/$directory/$file/index.html\">$file</a></div></div>"
      fi    
      content+="<div class=\"col-sm-2\"><div class=\"type\"><i class=\"far fa-folder\"></i> Directory</div></div>"
      content+="</div>"
      content+="<hr/>"            
    elif [ -f "$directory/$file" ]; then
      # file_type=$(file -b "$directory/$file")
      file_size=$(du -h "$directory/$file" | cut -f1)
      file_date=$(date -I -r "$directory/$file")
      content+="<div class=\"row\">"
      if [ "$directory" == "." ]; then
        content+="<div class=\"col-sm-8\"><div class=\"file\"><a href=\"$root_directory/$file\">$file</a></div></div>"
      else
        content+="<div class=\"col-sm-8\"><div class=\"file\"><a href=\"$root_directory/$directory/$file\">$file</a></div></div>"
      fi    
      content+="<div class=\"col-sm-2\"><div class=\"type\"> $file_date </div></div>"  
      content+="<div class=\"col-sm-2\"><div class=\"type\"><i class=\"far fa-hdd\"></i> $file_size</div></div>"     
      content+="</div>"      
      content+="<div class=\"row\">"            
      # content+="<div class=\"col-sm-12\"><div class=\"type\"><i class=\" icon-info-sign\"></i> $file_type</div></div>"                  
      content+="</div>"      
      content+="<hr/>"      
    fi
  done < <(ls -1 --group-directories-first $directory | grep -v "index.html\|css\|img\|make_index.sh")

  # append content to the index file
  echo $content >> $index_file

  # append footer to the index file
  footer >> $index_file
done
